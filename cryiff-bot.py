#!/usr/bin/python3 -u

import discord
from discord.ext import commands
import aiohttp
import asyncio

from face_detect import face_detect

import json
import os
import random
import requests
import shutil
import sys
import time
import urllib

description = '''Cryiff bot.'''
cryiff = commands.Bot(command_prefix='cryiff ', description=description)

r_headers = {
    'User-Agent': 'cryiff/1.0 (by perkipanda@gmail.com)',
    'From': 'perkipanda@gmail.com'
}
phrase = ''

@cryiff.event
async def on_ready():
    print('Logged in as')
    print(cryiff.user.name)
    print(cryiff.user.id)
    print('------')
    await cryiff.say("hewwo!")

@cryiff.command(pass_context = True)
async def new(ctx):
    # Notify user of pending process
    message = await cryiff.say('Fetching some yiff... UwU')
    async with aiohttp.ClientSession() as session:
        # Define variables used
        searchURL = "https://e621.net/post/index.json"
        showURL = "https://e621.net/post/show.json"
        tags = "-my_little_pony -human -high_res -animated rating:e open_mouth"
        parameters = {'tags': tags, 'limit': '1'}
        global phrase

        # Start a timer to ensure we don't exceed e621's 2-requests-per-second limit
        start = time.time()

        # Get highest possible ID
        async with session.get(url = searchURL, params = parameters, headers = r_headers) as res:
            if (await res.json() == [] or res.status != 200):
                await cryiff.say('e621 gave me bad data :(')
                print(res.json())
                return
            r_json = await res.json()
        max_id = r_json[0]['id']

        # Generate new random ID and search again to get the nearest valid image
        rand_id = random.randint(1, max_id)
        parameters['before_id'] = str(rand_id)
        async with session.get(url = searchURL, params = parameters, headers = r_headers) as res:
            if (await res.json() == [] or res.status != 200):
                await cryiff.say('e621 gave me bad data :(')
                print(res.json())
                return
            r_json = await res.json()
        valid_id = r_json[0]['id']

        # Check time as we've made 2 requests, and wait if necessary
        end = time.time()
        if (end - start < 1):
            await asyncio.sleep(end - start)

        # Pull post data at specified ID, then download
        parameters = {'id': str(valid_id)}
        response = requests.get(url = showURL, params = parameters, headers = r_headers)
        r_json = response.json()
        f_name = 'image.' + r_json['file_ext']
        image = open(f_name, 'wb')
        image.write(urllib.request.urlopen(r_json['file_url']).read())
        image.close()

    # Update message
    message = await cryiff.edit_message(message, 'Yiff fetched, cropping! OwO')

    # Feed image into face-detector
    await face_detect.detect(f_name)

    # Build a random phrase
    num_phrase_parts = random.randint(1, 2)
    phrase = "When "
    phrase += random.choice(open('database/nouns.txt').readlines()).rstrip()
    phrase += " "

    if num_phrase_parts == 1:
        phrase += random.choice(open('database/verbs_1.txt').readlines()).rstrip()
    else:
        phrase += random.choice(open('database/verbs_2.txt').readlines()).rstrip()
        phrase += " " + random.choice(open('database/nouns.txt').readlines()).rstrip()

    # Remove status message
    await cryiff.delete_message(message)

    # Upload first result to discord and delete files
    await cryiff.send_file(destination=ctx.message.channel, fp='face0.png', content=phrase)
    await cryiff.say("https://e621.net/post/show/" + str(valid_id))
    os.remove(f_name)

@cryiff.command(pass_context = True)
async def save(ctx):
    global phrase
    # Check if there's information to be dumped
    if phrase and os.path.isfile('face0.png'):
        # Dump phrase and face
        num = len([name for name in os.listdir('saved') if (os.path.isfile(os.path.join('saved', name)) and name.startswith('saved') and name.endswith('.txt'))])
        phrase_f = open(os.path.join('saved', 'saved' + str(num) + '.txt'), 'w')
        phrase_f.writelines(phrase)
        phrase_f.close()

        # Clean cached settings
        phrase = ''
        os.rename('face0.png', os.path.join('saved', 'saved' + str(num) + '.png'))

        # Say it was dumped
        await cryiff.say("Saved OwO")
    else:
        await cryiff.say("There's nothing to save UmU")

@cryiff.command(pass_context = True)
async def load(ctx):
    # Pick a random saved message and reupload it.
    num = len([name for name in os.listdir('saved') if (os.path.isfile(os.path.join('saved', name)) and name.startswith('saved') and name.endswith(".txt"))])
    if(num > 0):
        num = random.randint(0, num - 1)
        phrase_f = open(os.path.join('saved', 'saved' + str(num) + '.txt'), 'r')
        phrase = phrase_f.readline()
        await cryiff.send_file(destination=ctx.message.channel, fp=os.path.join('saved', 'saved' + str(num) + '.png'), content=phrase)
    else:
        await cryiff.say("You haven't saved anything UmU")

@cryiff.command(pass_context = True)
async def clean(ctx):
    await cryiff.purge_from(ctx.message.channel, limit=100)
    await cryiff.say("Messages purged UwU")

@cryiff.command(pass_context = True)
async def shutdown():
    await cryiff.say("Goodbye!")
    if os.path.isfile('face0.png'): os.remove('face0.png')
    await cryiff.logout()
    sys.exit(0)

@cryiff.command(pass_context = True)
async def add_noun(ctx, phrase):
    with open('database/nouns.txt', 'r') as nouns_file:
        lines = nouns_file.readlines()
        if any('{0}\n'.format(phrase) == line for line in lines):
            await cryiff.say("Phrase already exists.")
        else:
            lines.append('{0}\n'.format(phrase))
            nouns_file.close()
            with open('database/nouns.txt', 'w') as nouns_file:
                for line in lines:
                    nouns_file.write(line)
            await cryiff.say("Added {0} to nouns.".format(phrase))

@cryiff.event
async def on_command_error(error, ctx):
    await cryiff.send_message(ctx.message.channel, "I don't feel so good unu")
    await cryiff.send_message(ctx.message.channel, "```{0}```".format(error))

cryiff.run('NDQ3MjM1NDY4NzcwMjc5NDI1.DeEoDQ.R515ksLfBfLRfyeGCSbjp7enI4Q')
