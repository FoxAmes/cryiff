import cv2
import sys
import os.path

async def detect(filename, cascade_file = "./face_detect/cascade.xml"):
    if not os.path.isfile(cascade_file):
        raise RuntimeError("%s: not found" % cascade_file)

    cascade = cv2.CascadeClassifier(cascade_file)
    image = cv2.imread(filename, cv2.IMREAD_COLOR)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.equalizeHist(gray)
    
    faces = cascade.detectMultiScale(gray,
                                     # detector options
                                     scaleFactor = 1.3,
                                     minNeighbors = 15,
                                     minSize = (80, 80))
                                     
    face_num = 0
    for (x, y, w, h) in faces:
        crop_img = image[y:y+h, x:x+w]
        file_name = "face"
        file_name += str(face_num)
        file_name += ".png"
        cv2.imwrite(file_name, crop_img)
        face_num += 1
        break
